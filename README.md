# README #

Summary: 
Create a basic vehicle configurator using the provided art assets while adhering to the Configurator, Code, and UI Requirements listed below.

Assets needed:

1. Unity 3D v5.5+

2. Art assets included in root folder as a Unity package

3. SQLite

* Configurator Requirements:
    * Only need to target Windows x86
    * Implement camera mouse orbit (see Code Requirements)
    * Allow selection of the individual part categories of the vehicle by left-clicking on them (categories listed in Code Requirements)
        * User can click on any part of the vehicle to select that part
        * If the user selects a part with one already selected, deselect the first part
        * If the user clicks on nothing (not the vehicle or the UI), deselect any current selection
        * Must have a visual selection indicator (in addition to GUI) so the user knows what part is currently selected
            * E.g.: Outline shader, material swap, lighting, etc.
        * After clicking a part on the vehicle, user should be able to select a new part from those available for that category
            * E.g.: User selects front bumper, they are then presented the three (or less according to rules) available front bumper parts and can choose a new one
        * Allow deselection (having nothing selected)
    * Configuration must adhere to the following rules:
        * Mil_1 body is not compatible with Mil_2 back armor
        * Mil_3 decoration is not compatible with Mil_3 front armor
        * Mil_3 decoration is not compatible with Mil_2 body
        * Convey part incompatibility to the user
            * E.g.: Highlight part red, show warning modal/message, etc.
    * Allow saving configurations to a local SQLite table
        * Upon clicking “Save” button, a “Save Configuration” modal window should popup
        * Each configuration should include a name that the user inputs in the modal
    * Allow ability to load a previously-saved configuration
        * Upon clicking “Load” button, a “Load Configuration” modal window should popup
        * User can iterate through saved configurations
        * User can load a selected configuration and it will update the vehicle in the scene
    * Allow the user to generate a random configuration (random parts across all categories, taking rules into account)

 
* Code Requirements
    * Written entirely in C# (no UnityScript/JavaScript/Boo)
    * We don’t mind if you use a few community-provided scripts, but please cite your sources
    * Must implement an orbiting camera
        * Only allow orbiting while holding right mouse button, otherwise don’t move the camera when moving the mouse
        * Allow zooming with mouse wheel
    * Implement subscribed  Action(s) somewhere in the application
        * E.g.: OnSaveComplete, OnPartSelected, etc.
    * o	Utilize all 14 vehicle part categories and parts (each category has 3 parts)
        * ♣	Body
        * ♣	Front armor
        * ♣	Back armor
        * ♣	Front bumper
        * ♣	Back bumper
        * ♣	Front lights
        * ♣	Back lights
        * ♣	Top armor
        * ♣	Wheels
        * ♣	Suspension
        * ♣	Spoiler
        * ♣	Motor
        * ♣	Decoration
        * ♣	Brakes
    * Must use SQLite for saving/loading configurations
        * Table layout (schema) is up to you, just ensure the user can save a configuration to the table and later be able to load it
        * Must include a user-defined name for each configuration
    * Comment your code
        * At least a brief summary for each class/struct/interface and methods

* UI Requirements:
    * Any UI package should work but the UnityGUI system is preferred.
    * Provide interface to swap all configurable parts
    * Visual indicator of currently-selected category and part
        * E.g.: Label with category and part name
    * “Save” button and modal window
        * Modal window must include an Input field to enter the configuration name
        * Modal window must include a “Save” button and a Cancel/Close(X) button
    * “Load” button and modal window
        * Modal window must include a way to iterate through the saved configurations and load a selected one
        * Modal window must include a “Load” button and a Cancel/Close(X) button
    * Modal windows should not allow interaction with anything other than the modal itself
    * Random” button to generate a random configuration

 
* Deliverables:
    * Compressed project folder containing Assets and ProjectSettings folders only (don’t need /Temp, /Library, /obj or the Mono/VS solution files)
    * Sharing the file via Dropbox is preferable
    * Evaluation parameters:
    * All listed tasks completed and correctly adhered to provided rules
    	* Clean, concise, and consistent code
    	* Project organization
    	* Overall project presentation

* Optional “bonus points” tasks:
    * Utilize Base64 encoding and/or JSON for database serialization/deserialization during Save/Load
    * Make the UI at least loosely follow the MVC design pattern
    * Make the application “juicy” (add some polish)
    * Use Interfaces in code
    * Keep draw calls under 175
    * Create a Settings screen so the user can adjust camera movement speeds using sliders
        * Save the settings to the local PlayerPrefs and load them at application start
    * Allow the user to select a pre-built template vehicle to start with
    * After configuration is complete, allow user to drive the vehicle


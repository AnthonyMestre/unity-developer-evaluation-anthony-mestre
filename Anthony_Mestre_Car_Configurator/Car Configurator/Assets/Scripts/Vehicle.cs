﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle : MonoBehaviour {

    public GameObject  body, frontArmor, backArmor, frontBumper,
                        backBumper, frontLights, backLights, topArmor,
                        suspension, spoiler, motor, decoration;

    public GameObject[] wheels;
    public GameObject[] brakes;


    // Use this for initialization
    void Start ()
    {
        
    }
    
    // Update is called once per frame
    void Update ()
    {
        
    }
    public Part[] GetAllParts()
    {
        List<Part> parts = new List<Part>();

        parts.Add(body.GetComponent<Part>());
        parts.Add(frontArmor.GetComponent<Part>());
        parts.Add(backArmor.GetComponent<Part>());
        parts.Add(frontBumper.GetComponent<Part>());
        parts.Add(backBumper.GetComponent<Part>());
        parts.Add(frontLights.GetComponent<Part>());
        parts.Add(backLights.GetComponent<Part>());
        parts.Add(topArmor.GetComponent<Part>());
        parts.Add(suspension.GetComponent<Part>());
        parts.Add(spoiler.GetComponent<Part>());
        parts.Add(motor.GetComponent<Part>());
        parts.Add(decoration.GetComponent<Part>());
        parts.Add(wheels[0].GetComponent<Part>());
        parts.Add(brakes[0].GetComponent<Part>());

        return parts.ToArray();
    }

    public void SetPart(Part part)
    {

        if (part.type == Part.PartType.body)
            body = part.gameObject;
        else if (part.type == Part.PartType.frontArmor)
            frontArmor = part.gameObject;
        else if (part.type == Part.PartType.backArmor)
            backArmor = part.gameObject;
        else if (part.type == Part.PartType.frontBumper)
            frontBumper = part.gameObject;
        else if (part.type == Part.PartType.backBumper)
            backBumper = part.gameObject;
        else if (part.type == Part.PartType.frontLights)
            frontLights = part.gameObject;
        else if (part.type == Part.PartType.backLights)
            backLights = part.gameObject;
        else if (part.type == Part.PartType.topArmor)
            topArmor = part.gameObject;
        else if (part.type == Part.PartType.suspension)
            suspension = part.gameObject;

        else if (part.type == Part.PartType.spoiler)
            spoiler = part.gameObject;
        else if (part.type == Part.PartType.motor)
            motor = part.gameObject;
        else if (part.type == Part.PartType.decoration)
            decoration = part.gameObject;
        else if (part.type == Part.PartType.brake)
        {
            UpdateAllBrakes(part.gameObject);
        }
        else if (part.type == Part.PartType.wheel)
        {
            UpdateAllWheels(part.gameObject);
        }

    }

    public void SetPartToSelectedMaterial(Part part, Material material)
    {
        if (part.type == Part.PartType.wheel)
        {
            foreach(var w in wheels)
            {
                w.GetComponent<Part>().SetMaterial(material);
            }
        }
        else if (part.type == Part.PartType.brake)
        {
            foreach(var b in brakes)
            {
                b.GetComponent<Part>().SetMaterial(material);
            }
        }
        else
        {
            part.SetMaterial(material);
        }
    }

    public void RevertToOriginalWheelMaterial()
    {
        foreach(var w in wheels)
        {
            w.GetComponent<Part>().RevertToOriginalMaterial();
        }
    }

    public void RevertToOriginalBrakeMaterial()
    {
        foreach (var b in brakes)
        {
            b.GetComponent<Part>().RevertToOriginalMaterial();
        }
    }
    public void DestroyAllParts()
    {
        DestroyImmediate(body);
        DestroyImmediate(backArmor);
        DestroyImmediate(frontArmor);
        DestroyImmediate(frontBumper);
        DestroyImmediate(backBumper);
        DestroyImmediate(frontLights);
        DestroyImmediate(backLights);
        DestroyImmediate(topArmor);
        DestroyImmediate(suspension);
        DestroyImmediate(spoiler);
        DestroyImmediate(motor);
        DestroyImmediate(decoration);

        foreach(var w in wheels)
        {
            DestroyImmediate(w);
        }

        foreach(var b in brakes)
        {
            DestroyImmediate(b);
        }
    }

    public void DestroyPart(Part part)
    {
        if(part.type == Part.PartType.wheel)
        {
            foreach(var w in wheels)
            {
                DestroyImmediate(w);
            }
        }
        else if(part.type == Part.PartType.brake)
        {
            foreach (var b in brakes)
            {
                DestroyImmediate(b);
            }
        }
        else
            DestroyImmediate(part.gameObject);
    }

    void UpdateAllWheels(GameObject newWheel)
    {
        wheels[0] = newWheel;

        for(int i = 1; i < 4; i++)
        {
            wheels[i] = Instantiate(newWheel, transform);
        }

        wheels[1].transform.position = new Vector3(wheels[1].transform.position.x * -1,
                                                       wheels[1].transform.position.y,
                                                       wheels[1].transform.position.z);

        wheels[2].transform.position = new Vector3(wheels[2].transform.position.x * -1,
                                                   wheels[2].transform.position.y,
                                                   wheels[2].transform.position.z * -1);

        wheels[3].transform.position = new Vector3(wheels[3].transform.position.x,
                                                   wheels[3].transform.position.y,
                                                   wheels[3].transform.position.z * -1);

        wheels[1].transform.Rotate(Vector3.up, 180);
        wheels[2].transform.Rotate(Vector3.up, 180);
    }

    void UpdateAllBrakes(GameObject newBrake)
    {
        brakes[0] = newBrake;

        for (int i = 1; i < 4; i++)
        {
            brakes[i] = Instantiate(newBrake, transform);
        }

        brakes[1].transform.position = new Vector3(brakes[1].transform.position.x * -1,
                                                       brakes[1].transform.position.y,
                                                       brakes[1].transform.position.z);

        brakes[2].transform.position = new Vector3(brakes[2].transform.position.x * -1,
                                                   brakes[2].transform.position.y,
                                                   brakes[2].transform.position.z * -1);

        brakes[3].transform.position = new Vector3(brakes[3].transform.position.x,
                                                   brakes[3].transform.position.y,
                                                   brakes[3].transform.position.z * -1);

        brakes[1].transform.Rotate(Vector3.up, 180);
        brakes[2].transform.Rotate(Vector3.up, 180);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadWindow : MonoBehaviour {
	public CarConfigurator CarConfigurator;

    public GameObject ListItemPrefab;
    public GameObject ContentPanel;
    public ToggleGroup ListToggleGroup;

    private List<GameObject> listItems;
    private GameObject selectedItem;

	// Use this for initialization
	void Start () {
    }

    void OnEnable()
    {
        selectedItem = null;

        if (listItems == null)
            listItems = new List<GameObject>();

        //clear the list first
        while(listItems.Count > 0)
        {
            GameObject objectToDestroy = listItems[0];
            listItems.Remove(objectToDestroy);

            Destroy(objectToDestroy);
        }

        LoadListItem[] vehicleListItems = CarConfigurator.DBManager.LoadVehicleListItems();

        // Create a list item for each vehicle
        foreach (var v in vehicleListItems)
        {
            GameObject newItem = Instantiate(ListItemPrefab) as GameObject;
            newItem.GetComponentInChildren<Text>().text = v.Name;
            newItem.GetComponent<LoadListItem>().Name = v.Name;
            newItem.GetComponent<LoadListItem>().Guid = v.Guid;
            newItem.transform.parent = ContentPanel.transform;
            newItem.transform.localScale = Vector3.one;
            newItem.GetComponent<Toggle>().group = ListToggleGroup;
            newItem.GetComponent<Toggle>().onValueChanged.AddListener(FileSelected);
            listItems.Add(newItem);
        }
    }

    void FileSelected(bool IsOn)
    {
        // Make sure it's only called once per toggle
        if(IsOn)
        {
            foreach(var l in listItems)
            {
                if(l.GetComponent<Toggle>().isOn)
                {
                    selectedItem = l;
                    break;
                }
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

	public void CancelPressed()
	{
		gameObject.SetActive(false);
	}

	public void LoadPressed()
	{
        if(selectedItem != null)
        {
            LoadListItem item = selectedItem.GetComponent<LoadListItem>();
            CarConfigurator.LoadVehicleFromDB(selectedItem.GetComponent<LoadListItem>().Guid);

            gameObject.SetActive(false);
        }
	}
}

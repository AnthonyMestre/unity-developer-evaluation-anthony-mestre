﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveWindow : MonoBehaviour {
	public CarConfigurator CarConfigurator;
	public Text ConfigurationName;
	public Text NameError;
	// Use this for initialization
	void Start () {
		NameError.gameObject.SetActive(false);
	}

	public void CancelPressed()
	{
		NameError.gameObject.SetActive(false);
		gameObject.SetActive(false);
	}

	// When Save button is pressed, call the carConfigurator's SaveVehicle function, sending the name that was input. 
	// If there's no name, show an error warning. 
	public void SavePressed()
	{
		if (ConfigurationName.text != string.Empty)
		{
			CarConfigurator.SaveVehicleToDB(ConfigurationName.text);

			gameObject.SetActive(false);

		}
		else
		{
			NameError.gameObject.SetActive(true);
		}
	}
}

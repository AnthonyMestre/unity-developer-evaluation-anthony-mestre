﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Part : MonoBehaviour {
	public enum PartType { body, frontArmor, backArmor, frontBumper,
					backBumper, frontLights, backLights, topArmor,
					suspension, spoiler, motor, decoration, brake, wheel };

	public enum PartModel { Mil_1, Mil_2, Mil_3 };

	public PartType type;
	public PartModel model;

	private Material originalMaterial;
	private MeshRenderer[] meshRenderers;

    void Awake ()
    {
        meshRenderers = gameObject.GetComponentsInChildren<MeshRenderer>();

        foreach (var m in meshRenderers)
        {
            if (m.name.Contains("Glass") == false)
            {
                originalMaterial = m.material;
            }
        }
    }
	
	public void RevertToOriginalMaterial()
	{
		SetMaterial(originalMaterial);
	}

	public void SetMaterial(Material material)
	{
		foreach (var m in meshRenderers)
		{
			if (m.name.Contains("Glass") == false)
			{
				m.material = material;
			}
		}
	}
}

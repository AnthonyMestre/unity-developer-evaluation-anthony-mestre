﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour {

	public GameObject CarConfigurator;
	public ToggleGroup OptionsToggleGroup;
	public Toggle[] Options;
	public Toggle CurrentOption;
	public Text PartSelectedText;
	public SaveWindow SaveWindow;
	public LoadWindow LoadWindow;

	public Action<Part.PartModel> onOptionSelected;

	// Use this for initialization
	void Start () {
		if (CarConfigurator == null)
			CarConfigurator = GameObject.Find("CarConfigurator");

		CarConfigurator.GetComponent<CarConfigurator>().onPartSelected += OnPartSelected;
		onOptionSelected += CarConfigurator.GetComponent<CarConfigurator>().OnOptionSelected;

		SaveWindow.gameObject.SetActive(false);
		LoadWindow.gameObject.SetActive(false);

	}
	
	// Update is called once per frame
	void Update () {

	}

	public void OnPartSelected(Part part)
	{
		if (part != null)
		{
			PartSelectedText.text = part.type.ToString().ToUpper();
			OptionsToggleGroup.gameObject.SetActive(true);

			// Enable all options at first
			foreach (var o in Options)
				o.interactable = true;

			SetOption(part.model);
		}
		else
		{
			PartSelectedText.text = "NONE";
			OptionsToggleGroup.gameObject.SetActive(false);
		}
	}

	public void DisableOption(Part.PartModel model)
	{
		if(model == Part.PartModel.Mil_1)
		{
			Options[0].interactable = false;
		}
		else if(model == Part.PartModel.Mil_2)
		{
			Options[1].interactable = false;
		}
		else if(model == Part.PartModel.Mil_3)
		{
			Options[2].interactable = false;
		}
	}

	public void Option1Selected()
	{

		CurrentOption = Options[0];
		onOptionSelected(Part.PartModel.Mil_1);
	}

	public void Option2Selected()
	{

		CurrentOption = Options[1];
		onOptionSelected(Part.PartModel.Mil_2);
	}

	public void Option3Selected()
	{

		CurrentOption = Options[2];
		onOptionSelected(Part.PartModel.Mil_3);
	}

	public void SetOption(Part.PartModel model)
	{
		OptionsToggleGroup.SetAllTogglesOff();
		if (model == Part.PartModel.Mil_1)
			CurrentOption = Options[0];
		else if (model == Part.PartModel.Mil_2)
			CurrentOption = Options[1];
		else if (model == Part.PartModel.Mil_3)
			CurrentOption = Options[2];

		CurrentOption.isOn = true;
	}

	public void RandomPressed()
	{
		CarConfigurator.GetComponent<CarConfigurator>().RandomizeCar();
	}

	public void SavePressed()
	{
		SaveWindow.gameObject.SetActive(true);
	}

	public void LoadPressed()
	{
		LoadWindow.gameObject.SetActive(true);
	}
}

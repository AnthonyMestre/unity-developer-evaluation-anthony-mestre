﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CarConfigurator : MonoBehaviour {

	public UI UserInterface;
	public DatabaseManager DBManager;
	public GameObject vehicle;
	public List<Part> parts;
	public Material selectedMaterial1;
	public Material selectedMaterial2;
	public Material selectedMaterial3;

	private Part selectedPart;

	public Action<Part> onPartSelected;

	// Use this for initialization
	void Start () {
		LoadParts();
        LoadInitialVehicle();

		selectedPart = null;
	}
	
	// Update is called once per frame
	void Update ()
	{
		// Check for left click input. If raycast hit's an object that's not UI, select that object.
		if(Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
		{
			RaycastHit hitInfo = new RaycastHit();
			bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);

			if(hit)
			{
				if (selectedPart != null)
					SetPartToOriginalMaterial(selectedPart);

				selectedPart = hitInfo.transform.gameObject.GetComponent<Part>();
				SetPartToSelectedMaterial(selectedPart);
				onPartSelected(selectedPart);

				ImplementBusinessRules();

			}
			else
			{
				if(selectedPart != null)
					SetPartToOriginalMaterial(selectedPart);

				selectedPart = null;
				onPartSelected(null);
			}
		}
	}

    // This function disables certain options, based on business rules
	void ImplementBusinessRules()
	{
		Vehicle vehicleScript = vehicle.GetComponent<Vehicle>();

		
		if (selectedPart.type == Part.PartType.body)
		{
			// If vehicle has MIL_2 Back Armor, disable MIL_1 Body option
			if (vehicleScript.backArmor.GetComponent<Part>().model == Part.PartModel.Mil_2)
			{
				UserInterface.DisableOption(Part.PartModel.Mil_1);
			}

			// If vehicle has MIL_3 Decoration, disable MIL_2 Body
			if (vehicleScript.decoration.GetComponent<Part>().model == Part.PartModel.Mil_3)
			{
				UserInterface.DisableOption(Part.PartModel.Mil_2);
			}
		}
		else if (selectedPart.type == Part.PartType.backArmor)
		{
			// If vehicle has MIL_1 Body, disable MIL_2 Back Armor
			if(vehicleScript.body.GetComponent<Part>().model == Part.PartModel.Mil_1)
			{
				UserInterface.DisableOption(Part.PartModel.Mil_2);
			}
		}
		else if (selectedPart.type == Part.PartType.decoration)
		{
			// If vehicle has MIL_3 Front Armor or MIL_2 body, disable MIL_3 Decoration
			if (vehicleScript.frontArmor.GetComponent<Part>().model == Part.PartModel.Mil_3 ||
				vehicleScript.body.GetComponent<Part>().model == Part.PartModel.Mil_2)
			{
				UserInterface.DisableOption(Part.PartModel.Mil_3);
			}
		}
		else if (selectedPart.type == Part.PartType.frontArmor)
		{
			// If vehicle has MIL_3 Decoration, disable MIL_3 Front Armor
			if (vehicleScript.decoration.GetComponent<Part>().model == Part.PartModel.Mil_3)
			{
				UserInterface.DisableOption(Part.PartModel.Mil_3);
			}
		}

	}

    // Gets called when a vehicle part is clicked on, and set's the parts material to
    // a different material to show the selection. 
	void SetPartToSelectedMaterial(Part part)
	{
		Material selectedMaterial;
		if(part.model == Part.PartModel.Mil_1)
		{
			selectedMaterial = selectedMaterial1;
		}
		else if(part.model == Part.PartModel.Mil_2)
		{
			selectedMaterial = selectedMaterial2;
		}
		else
		{
			selectedMaterial = selectedMaterial3;
		}

		vehicle.GetComponent<Vehicle>().SetPartToSelectedMaterial(part, selectedMaterial);
	}

    // Reverts Part Material to original Material. Wheels and brakes are handled uniquely
    // since there's four of them.
	void SetPartToOriginalMaterial(Part part)
	{
		if(part.type == Part.PartType.wheel)
		{
			vehicle.GetComponent<Vehicle>().RevertToOriginalWheelMaterial();
		}
		else if(part.type == Part.PartType.brake)
		{
			vehicle.GetComponent<Vehicle>().RevertToOriginalBrakeMaterial();
		}
		else
		{
			part.RevertToOriginalMaterial();
		}
	}

	// When a GUI Option is selected, Destroy the old part and Instantiate the new part.
	public void OnOptionSelected(Part.PartModel model)
	{
		if (selectedPart.model != model)
		{
			Part.PartType currentType = selectedPart.type;

			vehicle.GetComponent<Vehicle>().DestroyPart(selectedPart);

			GameObject newPart = Instantiate(parts.Where(p => p.model == model && p.type == currentType).First().gameObject, vehicle.transform);
			vehicle.GetComponent<Vehicle>().SetPart(newPart.GetComponent<Part>());
			SetPartToSelectedMaterial(newPart.GetComponent<Part>());

			selectedPart = newPart.GetComponent<Part>();
		}
	}

    // Calls Database manager's SaveVehicle, passing in all the parts and the vehicle name.
	public void SaveVehicleToDB(string vehicleName)
	{
		Vehicle vehicleScript = vehicle.GetComponent<Vehicle>();

		DBManager.SaveVehicle(vehicleScript.GetAllParts(), vehicleName);
	}

    // Calls DBManager's LoadVehicleParts. It then uses the parts to Instantiate the parts,
    // and recreate the vehicle using the new parts.
    public void LoadVehicleFromDB(string vehicleId)
    {
        PartDBModel[] loadedParts = DBManager.LoadVehicleParts(vehicleId);

        Vehicle vehicleScript = vehicle.GetComponent<Vehicle>();

        // Set selection to null
        selectedPart = null;
        onPartSelected(null);

        // Destroy all vehicle parts
        vehicleScript.DestroyAllParts();

        foreach (var l in loadedParts)
        {
            GameObject partGO = Instantiate(parts.Where(p => p.type.ToString() == l.Type && p.model.ToString() == l.Model).First().gameObject, vehicle.transform);
            vehicleScript.SetPart(partGO.GetComponent<Part>());
        }
    }

    // Creates a random car following the business rules.
	public void RandomizeCar()
	{
		Vehicle vehicleScript = vehicle.GetComponent<Vehicle>();

		// Set selection to null
		selectedPart = null;
		onPartSelected(null);

		// Destroy all vehicle parts
		vehicleScript.DestroyAllParts();


		Part.PartModel randomModel = RandomModel();
		GameObject body = Instantiate(parts.Where(p => p.type == Part.PartType.body && p.model == randomModel).First().gameObject, vehicle.transform);

		randomModel = RandomModel();
		GameObject frontArmor = Instantiate(parts.Where(p => p.type == Part.PartType.frontArmor && p.model == randomModel).FirstOrDefault().gameObject, vehicle.transform);

		randomModel = RandomModel();
		GameObject frontBumper = Instantiate(parts.Where(p => p.type == Part.PartType.frontBumper && p.model == randomModel).FirstOrDefault().gameObject, vehicle.transform);

		randomModel = RandomModel();
		GameObject backBumper = Instantiate(parts.Where(p => p.type == Part.PartType.backBumper && p.model == randomModel).FirstOrDefault().gameObject, vehicle.transform);

		randomModel = RandomModel();
		GameObject frontLights = Instantiate(parts.Where(p => p.type == Part.PartType.frontLights && p.model == randomModel).FirstOrDefault().gameObject, vehicle.transform);

		randomModel = RandomModel();
		GameObject backLights = Instantiate(parts.Where(p => p.type == Part.PartType.backLights && p.model == randomModel).FirstOrDefault().gameObject, vehicle.transform);

		randomModel = RandomModel();
		GameObject topArmor = Instantiate(parts.Where(p => p.type == Part.PartType.topArmor && p.model == randomModel).FirstOrDefault().gameObject, vehicle.transform);

		randomModel = RandomModel();
		GameObject wheels = Instantiate(parts.Where(p => p.type == Part.PartType.wheel && p.model == randomModel).FirstOrDefault().gameObject, vehicle.transform);

		randomModel = RandomModel();
		GameObject suspension = Instantiate(parts.Where(p => p.type == Part.PartType.suspension && p.model == randomModel).FirstOrDefault().gameObject, vehicle.transform);

		randomModel = RandomModel();
		GameObject spoiler = Instantiate(parts.Where(p => p.type == Part.PartType.spoiler && p.model == randomModel).FirstOrDefault().gameObject, vehicle.transform);

		randomModel = RandomModel();
		GameObject motor = Instantiate(parts.Where(p => p.type == Part.PartType.motor && p.model == randomModel).FirstOrDefault().gameObject, vehicle.transform);

		randomModel = RandomModel();
		GameObject brakes = Instantiate(parts.Where(p => p.type == Part.PartType.brake && p.model == randomModel).FirstOrDefault().gameObject, vehicle.transform);

		//// If body is MIL_1 keep "Rerolling" the random model until it's not MIL_2
		randomModel = RandomModel();
		if (body.GetComponent<Part>().model == Part.PartModel.Mil_1)
		{

			while (randomModel == Part.PartModel.Mil_2)
			{
				randomModel = RandomModel();
			}
		}
		GameObject backArmor = Instantiate(parts.Where(p => p.type == Part.PartType.backArmor && p.model == randomModel).First().gameObject, vehicle.transform);

		//// If body is MIL_2 or front Armor is MIL_3, keep "Rerolling" the random model until it's not MIL_3
		randomModel = RandomModel();
		if (body.GetComponent<Part>().model == Part.PartModel.Mil_2 || frontArmor.GetComponent<Part>().model == Part.PartModel.Mil_3)
		{
			while (randomModel == Part.PartModel.Mil_3)
			{
				randomModel = RandomModel();
			}
		}
		GameObject decoration = Instantiate(parts.Where(p => p.type == Part.PartType.decoration && p.model == randomModel).First().gameObject, vehicle.transform);



		vehicleScript.SetPart(body.GetComponent<Part>());
		vehicleScript.SetPart(frontArmor.GetComponent<Part>());
		vehicleScript.SetPart(frontBumper.GetComponent<Part>());
		vehicleScript.SetPart(backBumper.GetComponent<Part>());
		vehicleScript.SetPart(frontLights.GetComponent<Part>());
		vehicleScript.SetPart(backLights.GetComponent<Part>());
		vehicleScript.SetPart(topArmor.GetComponent<Part>());
		vehicleScript.SetPart(wheels.GetComponent<Part>());
		vehicleScript.SetPart(suspension.GetComponent<Part>());
		vehicleScript.SetPart(spoiler.GetComponent<Part>());
		vehicleScript.SetPart(motor.GetComponent<Part>());
		vehicleScript.SetPart(brakes.GetComponent<Part>());
		vehicleScript.SetPart(backArmor.GetComponent<Part>());
		vehicleScript.SetPart(decoration.GetComponent<Part>());

	}

    // Helper function for RandomizeCar().
	Part.PartModel RandomModel()
	{
		int randomNumber = UnityEngine.Random.Range(0, 3);

		if (randomNumber == 0)
			return Part.PartModel.Mil_1;
		else if (randomNumber == 1)
			return Part.PartModel.Mil_2;
		else
			return Part.PartModel.Mil_3;

	}

    // Loads all the parts from the resource folder, so that they're ready to be instantiated.
	void LoadParts()
	{
		GameObject mil1_body =          Resources.Load("VehiclePartPrefabs/Mil_1_Body_1") as GameObject;
		GameObject mil1_backArmor =     Resources.Load("VehiclePartPrefabs/Mil_1_Armor_B_1") as GameObject;
		GameObject mil1_backBumper =    Resources.Load("VehiclePartPrefabs/Mil_1_Bumper_B_1") as GameObject;
		GameObject mil1_backLights =    Resources.Load("VehiclePartPrefabs/Mil_1_Light_B_2") as GameObject;
		GameObject mil1_brakes =        Resources.Load("VehiclePartPrefabs/Mil_1_Brake_1") as GameObject;
		GameObject mil1_decoration =    Resources.Load("VehiclePartPrefabs/Mil_1_Deco_1") as GameObject;
		GameObject mil1_frontArmor =    Resources.Load("VehiclePartPrefabs/Mil_1_Armor_F_1") as GameObject;
		GameObject mil1_frontBumper =   Resources.Load("VehiclePartPrefabs/Mil_1_Bumper_F_1") as GameObject;
		GameObject mil1_frontLights =   Resources.Load("VehiclePartPrefabs/Mil_1_Light_F_1") as GameObject;
		GameObject mil1_motor =         Resources.Load("VehiclePartPrefabs/Mil_1_Motor_1") as GameObject;
		GameObject mil1_spoiler =       Resources.Load("VehiclePartPrefabs/Mil_1_Spoiler_1") as GameObject;
		GameObject mil1_suspension =    Resources.Load("VehiclePartPrefabs/Mil_1_Suspension_1") as GameObject;
		GameObject mil1_topArmor =      Resources.Load("VehiclePartPrefabs/Mil_1_Back_1") as GameObject;
		GameObject mil1_wheel =         Resources.Load("VehiclePartPrefabs/Mil_1_Wheel_1") as GameObject;

		GameObject mil2_body =          Resources.Load("VehiclePartPrefabs/Mil_2_Body_2") as GameObject;
		GameObject mil2_backArmor =     Resources.Load("VehiclePartPrefabs/Mil_2_Armor_B_2") as GameObject;
		GameObject mil2_backBumper =    Resources.Load("VehiclePartPrefabs/Mil_2_Bumper_B_2") as GameObject;
		GameObject mil2_backLights =    Resources.Load("VehiclePartPrefabs/Mil_2_Light_B_2") as GameObject;
		GameObject mil2_brakes =        Resources.Load("VehiclePartPrefabs/Mil_2_Brake_2") as GameObject;
		GameObject mil2_decoration =    Resources.Load("VehiclePartPrefabs/Mil_2_Deco_2") as GameObject;
		GameObject mil2_frontArmor =    Resources.Load("VehiclePartPrefabs/Mil_2_Armor_F_2") as GameObject;
		GameObject mil2_frontBumper =   Resources.Load("VehiclePartPrefabs/Mil_2_Bumper_F_2") as GameObject;
		GameObject mil2_frontLights =   Resources.Load("VehiclePartPrefabs/Mil_2_Light_F_2") as GameObject;
		GameObject mil2_motor =         Resources.Load("VehiclePartPrefabs/Mil_2_Motor_2") as GameObject;
		GameObject mil2_spoiler =       Resources.Load("VehiclePartPrefabs/Mil_2_Spoiler_2") as GameObject;
		GameObject mil2_suspension =    Resources.Load("VehiclePartPrefabs/Mil_2_Suspension_2") as GameObject;
		GameObject mil2_topArmor =      Resources.Load("VehiclePartPrefabs/Mil_2_Back_2") as GameObject;
		GameObject mil2_wheel =         Resources.Load("VehiclePartPrefabs/Mil_2_Wheel_2") as GameObject;

		GameObject mil3_body =          Resources.Load("VehiclePartPrefabs/Mil_3_Body_3") as GameObject;
		GameObject mil3_backArmor =     Resources.Load("VehiclePartPrefabs/Mil_3_Armor_B_3") as GameObject;
		GameObject mil3_backBumper =    Resources.Load("VehiclePartPrefabs/Mil_3_Bumper_B_3") as GameObject;
		GameObject mil3_backLights =    Resources.Load("VehiclePartPrefabs/Mil_3_Light_B_3") as GameObject;
		GameObject mil3_brakes =        Resources.Load("VehiclePartPrefabs/Mil_3_Brake_3") as GameObject;
		GameObject mil3_decoration =    Resources.Load("VehiclePartPrefabs/Mil_3_Deco_3") as GameObject;
		GameObject mil3_frontArmor =    Resources.Load("VehiclePartPrefabs/Mil_3_Armor_F_3") as GameObject;
		GameObject mil3_frontBumper =   Resources.Load("VehiclePartPrefabs/Mil_3_Bumper_F_3") as GameObject;
		GameObject mil3_frontLights =   Resources.Load("VehiclePartPrefabs/Mil_3_Light_F_3") as GameObject;
		GameObject mil3_motor =         Resources.Load("VehiclePartPrefabs/Mil_3_Motor_3") as GameObject;
		GameObject mil3_spoiler =       Resources.Load("VehiclePartPrefabs/Mil_3_Spoiler_3") as GameObject;
		GameObject mil3_suspension =    Resources.Load("VehiclePartPrefabs/Mil_3_Suspension_3") as GameObject;
		GameObject mil3_topArmor =      Resources.Load("VehiclePartPrefabs/Mil_3_Back_3") as GameObject;
		GameObject mil3_wheel =         Resources.Load("VehiclePartPrefabs/Mil_3_Wheel_3") as GameObject;

		parts.Add(mil1_body.GetComponent<Part>());
		parts.Add(mil1_backArmor.GetComponent<Part>());
		parts.Add(mil1_backBumper.GetComponent<Part>());
		parts.Add(mil1_backLights.GetComponent<Part>());
		parts.Add(mil1_brakes.GetComponent<Part>());
		parts.Add(mil1_decoration.GetComponent<Part>());
		parts.Add(mil1_frontArmor.GetComponent<Part>());
		parts.Add(mil1_frontBumper.GetComponent<Part>());
		parts.Add(mil1_frontLights.GetComponent<Part>());
		parts.Add(mil1_motor.GetComponent<Part>());
		parts.Add(mil1_spoiler.GetComponent<Part>());
		parts.Add(mil1_suspension.GetComponent<Part>());
		parts.Add(mil1_topArmor.GetComponent<Part>());
		parts.Add(mil1_wheel.GetComponent<Part>());

		parts.Add(mil2_body.GetComponent<Part>());
		parts.Add(mil2_backArmor.GetComponent<Part>());
		parts.Add(mil2_backBumper.GetComponent<Part>());
		parts.Add(mil2_backLights.GetComponent<Part>());
		parts.Add(mil2_brakes.GetComponent<Part>());
		parts.Add(mil2_decoration.GetComponent<Part>());
		parts.Add(mil2_frontArmor.GetComponent<Part>());
		parts.Add(mil2_frontBumper.GetComponent<Part>());
		parts.Add(mil2_frontLights.GetComponent<Part>());
		parts.Add(mil2_motor.GetComponent<Part>());
		parts.Add(mil2_spoiler.GetComponent<Part>());
		parts.Add(mil2_suspension.GetComponent<Part>());
		parts.Add(mil2_topArmor.GetComponent<Part>());
		parts.Add(mil2_wheel.GetComponent<Part>());

		parts.Add(mil3_body.GetComponent<Part>());
		parts.Add(mil3_backArmor.GetComponent<Part>());
		parts.Add(mil3_backBumper.GetComponent<Part>());
		parts.Add(mil3_backLights.GetComponent<Part>());
		parts.Add(mil3_brakes.GetComponent<Part>());
		parts.Add(mil3_decoration.GetComponent<Part>());
		parts.Add(mil3_frontArmor.GetComponent<Part>());
		parts.Add(mil3_frontBumper.GetComponent<Part>());
		parts.Add(mil3_frontLights.GetComponent<Part>());
		parts.Add(mil3_motor.GetComponent<Part>());
		parts.Add(mil3_spoiler.GetComponent<Part>());
		parts.Add(mil3_suspension.GetComponent<Part>());
		parts.Add(mil3_topArmor.GetComponent<Part>());
		parts.Add(mil3_wheel.GetComponent<Part>());
	}


    // Loads an initial vehicle. To be called at the start of the app.
	void LoadInitialVehicle()
	{
		// TODO: CHANGE ALL OF THIS TO LOAD FROM DATABASE. 
		// IF NO VEHICLE IN DATABASE,THEN LOAD A DEFAULT VEHICLE.

		Vehicle vehicleScript = vehicle.GetComponent<Vehicle>();

		if(vehicleScript != null)
		{
			vehicleScript.body = Instantiate(parts.Where(p => p.type == Part.PartType.body).First().gameObject, vehicle.transform);
			vehicleScript.backArmor = Instantiate(parts.Where(p => p.type == Part.PartType.backArmor).First().gameObject, vehicle.transform);
			vehicleScript.backBumper = Instantiate(parts.Where(p => p.type == Part.PartType.backBumper).First().gameObject, vehicle.transform);
			vehicleScript.backLights = Instantiate(parts.Where(p => p.type == Part.PartType.backLights).First().gameObject, vehicle.transform);
			vehicleScript.decoration = Instantiate(parts.Where(p => p.type == Part.PartType.decoration).First().gameObject, vehicle.transform);
			vehicleScript.frontArmor = Instantiate(parts.Where(p => p.type == Part.PartType.frontArmor).First().gameObject, vehicle.transform);
			vehicleScript.frontBumper = Instantiate(parts.Where(p => p.type == Part.PartType.frontBumper).First().gameObject, vehicle.transform);
			vehicleScript.frontLights = Instantiate(parts.Where(p => p.type == Part.PartType.frontLights).First().gameObject, vehicle.transform);
			vehicleScript.motor = Instantiate(parts.Where(p => p.type == Part.PartType.motor).First().gameObject, vehicle.transform);
			vehicleScript.spoiler = Instantiate(parts.Where(p => p.type == Part.PartType.spoiler).First().gameObject, vehicle.transform);
			vehicleScript.suspension = Instantiate(parts.Where(p => p.type == Part.PartType.suspension).First().gameObject, vehicle.transform);
			vehicleScript.topArmor = Instantiate(parts.Where(p => p.type == Part.PartType.topArmor).First().gameObject, vehicle.transform);
			vehicleScript.wheels[0] = Instantiate(parts.Where(p => p.type == Part.PartType.wheel).First().gameObject, vehicle.transform);
			vehicleScript.wheels[1] = Instantiate(vehicleScript.wheels[0], vehicle.transform) as GameObject;
			vehicleScript.wheels[2] = Instantiate(vehicleScript.wheels[0], vehicle.transform) as GameObject;
			vehicleScript.wheels[3] = Instantiate(vehicleScript.wheels[0], vehicle.transform) as GameObject;

			vehicleScript.wheels[1].transform.position = new Vector3(vehicleScript.wheels[1].transform.position.x*-1,
																	vehicleScript.wheels[1].transform.position.y,
																	vehicleScript.wheels[1].transform.position.z);

			vehicleScript.wheels[2].transform.position = new Vector3(vehicleScript.wheels[2].transform.position.x * -1,
																	vehicleScript.wheels[2].transform.position.y,
																	vehicleScript.wheels[2].transform.position.z * -1);

			vehicleScript.wheels[3].transform.position = new Vector3(vehicleScript.wheels[3].transform.position.x,
																	vehicleScript.wheels[3].transform.position.y,
																	vehicleScript.wheels[3].transform.position.z * -1);

            vehicleScript.wheels[1].transform.Rotate(Vector3.up, 180);
            vehicleScript.wheels[2].transform.Rotate(Vector3.up, 180);

            vehicleScript.brakes[0] = Instantiate(parts.Where(p => p.type == Part.PartType.brake).First().gameObject, vehicle.transform);
			vehicleScript.brakes[1] = Instantiate(vehicleScript.brakes[0], vehicle.transform) as GameObject;
			vehicleScript.brakes[2] = Instantiate(vehicleScript.brakes[0], vehicle.transform) as GameObject;
			vehicleScript.brakes[3] = Instantiate(vehicleScript.brakes[0], vehicle.transform) as GameObject;

			vehicleScript.brakes[1].transform.position = new Vector3(vehicleScript.brakes[1].transform.position.x * -1,
																	vehicleScript.brakes[1].transform.position.y,
																	vehicleScript.brakes[1].transform.position.z);

			vehicleScript.brakes[2].transform.position = new Vector3(vehicleScript.brakes[2].transform.position.x * -1,
																	vehicleScript.brakes[2].transform.position.y,
																	vehicleScript.brakes[2].transform.position.z * -1);

			vehicleScript.brakes[3].transform.position = new Vector3(vehicleScript.brakes[3].transform.position.x,
																	vehicleScript.brakes[3].transform.position.y,
																	vehicleScript.brakes[3].transform.position.z * -1);

            vehicleScript.brakes[1].transform.Rotate(Vector3.up, 180);
            vehicleScript.brakes[2].transform.Rotate(Vector3.up, 180);
        }
	}

}

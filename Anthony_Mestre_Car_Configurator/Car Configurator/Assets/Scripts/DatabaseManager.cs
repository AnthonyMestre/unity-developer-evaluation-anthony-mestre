﻿using Mono.Data.Sqlite;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatabaseManager : MonoBehaviour {

	public CarConfigurator CarConfigurator;

	private IDbConnection dbconn;
	private IDbCommand dbcmd;
	private IDataReader reader;
	private string conn;

	// setting conn on awake to make sure it's set before anything else.
	void Awake ()
	{
		conn = "URI=file:" + Application.dataPath + "/CarConfiguratorDB.db"; //Path to database.
	}
    public PartDBModel[] LoadVehicleParts(string vehicleID)
    {
        List<PartDBModel> parts = new List<PartDBModel>();

        OpenDB();

        string sqlQuery =   "SELECT Type, Model " + 
                            "FROM Part " + 
                            "WHERE VehicleID = '" + vehicleID + "' ";
        dbcmd.CommandText = sqlQuery;
        reader = dbcmd.ExecuteReader();

        while (reader.Read())
        {
            string type = reader.GetString(0);
            string model = reader.GetString(1);

            Debug.Log("Type = " + type + ", Model = " + model);

            PartDBModel part = new PartDBModel();
            part.Model = model;
            part.Type = type;
            parts.Add(part);
        }

        CloseDB();


        return parts.ToArray();
    }
	public LoadListItem[] LoadVehicleListItems()
	{
		List<LoadListItem> listItems = new List<LoadListItem>();

        if (!String.IsNullOrEmpty(conn))
        {
            OpenDB();

            string sqlQuery = "SELECT ID, Name " + "FROM Vehicle ";
            dbcmd.CommandText = sqlQuery;
            reader = dbcmd.ExecuteReader();

            while (reader.Read())
            {
                string ID = reader.GetString(0);
                string name = reader.GetString(1);

                LoadListItem item = new LoadListItem();
                item.Name = name;
                item.Guid = ID;
                listItems.Add(item);
            }

            CloseDB();
        }

        return listItems.ToArray();
	}

	public void SaveVehicle(Part[] parts, string name)
	{
		Guid VehicleID = Guid.NewGuid();

        OpenDB();

        // Save to Vehicle table
        string sqlQuery = string.Empty;
		sqlQuery = "INSERT INTO Vehicle (ID, Name) " + "VALUES ('" + VehicleID.ToString() + "', '" + name + "'); ";
		dbcmd.CommandText = sqlQuery;
		reader = dbcmd.ExecuteReader();

        CloseDB();
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // Save all parts to parts table
        OpenDB();


		sqlQuery = string.Empty;
		foreach (var p in parts)
		{
			sqlQuery += "INSERT INTO Part (Type, Model, VehicleID) " + "VALUES ('" + p.type.ToString() + "', '" + p.model.ToString() + "', '" + VehicleID.ToString() + "'); ";

		}
		dbcmd.CommandText = sqlQuery;
		reader = dbcmd.ExecuteReader();

        CloseDB();


        Debug.Log(name + " saved!");
	}

    void OpenDB()
    {
        dbconn = (IDbConnection)new SqliteConnection(conn);
        dbconn.Open(); //Open connection to the database.
        dbcmd = dbconn.CreateCommand();
    }

    void CloseDB()
    {
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
}
